/* Wesley Ellington
 * Math 4370 
 * Homework 2
 * 2 Dimensional Vector Struct */

#include "vec2d.h"
#include "time.h"
#include <math.h>
#include <stdio.h>

/* Implementation of 2D vector structs
 * Based on a double ** data type */

/* Base constructor, allocates and zeros memory */
vec2d* vec2dNew(long int mlen, long int nlen){
	/* ensure lengths are legal*/
	int i;
	if (mlen < 1 || nlen < 1){
		fprintf(stderr, "vec2dNew error, illegal vector length");
		return NULL;
	}

	/* begin to allocate memory */

	/* allocate struct */
	vec2d *x = (vec2d*) malloc(sizeof(vec2d));
	
	/* if allocation fails, return null */
	if (x == NULL){
		return NULL;
	}
	
	/* assign struct vars */
	x->m = mlen;
	x->n = nlen;

	/* allocate data memory */
	x->data = (double**) malloc(mlen*sizeof(double*));
	for(i = 0; i < mlen; i++){
		x->data[i] = (double*) calloc(nlen, sizeof(double));
	}
	return x;
}

/* Linear spaced constructor */
vec2d* vec2dLinspace(double a, double b, 
	long int m, long int n){
	int i, j;
	/* create new m by n vector */
	vec2d* x = vec2dNew(m,n);

	/* return null ptr if input vector is null*/
	if(x == NULL){
		return NULL;
	}

	/* iterate over column, setting all values equal to
 	* linear spacing over m */ 	
	for(i = 0; i < m; i++){
		double val = a + (b-a)/(m-1)*i;
		for(j = 0; j < n; j++){
			x->data[i][j] = val;
		}
	}	
	return x;
}

/* Randomly populated vector constructor */
vec2d* vec2dRandom(long int m, long int n){
	/*create vector*/
	vec2d* x = vec2dNew(m,n);

	int i,j;
	
	if(x->data == NULL){
		fprintf(stderr, "Rand Vec not initialized!");
	}

	/*normalization constant*/
	double norm = pow(2.0,31.0) - 1.0;

	for(i = 0; i < m; i++){
		for(j = 0; j < n; j++){
			x->data[i][j] = random()/norm;
		}	
	}	

	return x;
}

/* Destructor, deallocates memory */
void vec2dDestroy(vec2d* inVec){
	int i;
	/* Dealloc internal arrays*/
	for(i = 0; i < inVec->m; i++){
		free(inVec->data[i]);
	}
	/* Dealloc outerlevel */
	free(inVec->data);
	/* Zero out values */
	inVec->m = 0;
	inVec->n = 0;
	
}

/* I/O functions */
/* Write to stdout*/
int vec2dWrite(vec2d* x){
	int i, j;
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			printf("%f ", x->data[i][j]);
		}
		printf("\n");
	}
	return 0;
}

/* Write to file */
int vec2dWriteFile(vec2d* x, const char* outfile){
	long int i, j;
	FILE * fptr = NULL;

	/* quite if vector has no alloced array*/
	if (x->data == NULL){
		fprintf(stderr, "Write to file error, no array\n");
		return 1;
	}

	/* return if no file string specified */
	if (strlen(outfile) < 1){
		fprintf(stderr, "Write for file error, no name\n");
		return 1;
	}

	/* open file and verify valid */
	fptr = fopen(outfile, "w");
	if(fptr == NULL){
		fprintf(stderr, "Write to file error, unable to open %s \n", outfile);
		return 1;
	}

	/* print to file */
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			fprintf(fptr, "%f ", x->data[i][j]);
		}
		fprintf(fptr, "\n");
	}

	/* close file */
	fclose(fptr);
	return 0;
} 

/* Linear sum function */
/* x = a*y + b*z */
int vec2dLinearSum(vec2d* x, double a, vec2d* y,
		double b, vec2d* z ){
	int i, j;
	/*error if data is unreachable or sizes are incompatable*/	
	if(x->data == NULL || y->data == NULL || z->data ==NULL){
		fprintf(stderr, "vec2dLinsum error, null data\n");
		return 1;
	}

	if(x->m != y->m || x->m != z->m || x->n != y->n || x->n != z->n){
		fprintf(stderr, "vec2dLinsum error, diff sized arrays\n");
		return 1;
	}
	
	/* calculate values for each iteration*/
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			x->data[i][j] = y->data[i][j] * a + z->data[i][j] * b;
		}
	}


	return 0;
}

/* Scale function */
/* x = a*x */
int vec2dScale(vec2d* x, double a){
	int i,j;

	if(x->data == NULL){
		fprintf(stderr, "vec2dScale error, null data\n");
		return 1;
	}
	
	/*iterate over matix, scale each entry*/
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			x->data[i][j] = x->data[i][j] * a;
		}
	}

	return 0;
}

/* Vector copy */
/* x = y */
int vec2dCopy(vec2d* x, vec2d* y){
	int i, j;

	/*return with error for null data and diff sizes*/
	if(x->data == NULL || y-> data == NULL){
		fprintf(stderr, "vec2dCopy error, null data array");
		return 1;
	}
	
	if(x->m != y->m || x->n != y->n){
		fprintf(stderr, "vec2dCopy error, diffe sized arrays");
		return 1;
	}

	/*copy elements */
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			x->data[i][j] = y->data[i][j];
		}
	}

	return 0;
}

/* Vector Constant */
/* sets all entries to a */
int vec2dConstant(vec2d* x, double a){
	int i,j;
	
	/*error if null data*/
	if(x->data == NULL){
		fprintf(stderr, "vec2dConstant error, null data array");
		return 1;
	}

	/* iterate over vector, set all entries to a */
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			x->data[i][j] = a;
		}
	}
	return 0;
}

/* Scalar values */

/*Minimum value*/
double vec2dMin(vec2d* x){
	int i, j;
	double min = x->data[0][0];

	/* replace min value with new lowest value*/
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			if(x->data[i][j] < min){
				min  = x->data[i][j];
			}
		}
	}
	return min;
}

/*Maximum value*/
double vec2dMax(vec2d* x){
	int i, j;
	double max = x->data[0][0];

	/*replace max value with new lowest value*/
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			if(x->data[i][j] > max){
				max  = x->data[i][j];
			}
		}
	}
	return max;
}

/*Dot product of two vectors*/
double vec2dDot(vec2d* x, vec2d* y){
	int i, j;
	double sum = 0.0;

	/* error if sizes are incompatable*/
	if(x->m != y->m || x->n != y->n){
		fprintf(stderr, "vec2dDot error, different sized vecs\n");
		sum = 0.0;
		return sum;
	}

	/* iterate over entries, multiply pairs, sum*/	
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			sum = sum + (x->data[i][j] * y->data[i][j]);
		}
	}	
	return sum;
}

/*Two norm of vector*/
double vec2dTwoNorm(vec2d* x){
	double dot = vec2dDot(x,x);
	return sqrt((double)dot);
}

/*Root mean square norm of vector*/
double vec2dRmsNorm(vec2d* x){
	double dot = vec2dDot(x,x);
	return sqrt((double)dot/(double)(x->m * x->n));
}

/*Maximum norm/inf norm*/
double vec2dMaxNorm(vec2d* x){
	int i, j;	
	double max = 0.0;
	for(i = 0; i < x->m; i++){
		for(j = 0; j < x->n; j++){
			max = (max > fabs(x->data[i][j])) ? max : fabs(x->data[i][j]);
		}
	}
	return max;
}
