/* Wesley Ellington
 * Math 4370 
 * Homework 2
 * 2 Dimensional Vector Struct 
 * Using 2D(** type) Vector memory*/

#ifndef VEC2D_DEFINED__
#define VEC2D_DEFINED__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/*Boolean values*/
#define True 1
#define False 0

/* Definition of member variables*/
typedef struct _vec2d{
	/*size variables*/
	long int m;
	long int n;
	
	/*value array*/
	double ** data;
} vec2d;

/* Base constructor, allocates and zeros memory */
vec2d* vec2dNew(long int mlen, long int nlen);

/* Linear spaced constructor */
vec2d* vec2dLinspace(double a, double b, 
	long int m, long int n);

/* Randomly populated vector constructor */
vec2d* vec2dRandom(long int m, long int n);

/* Destructor, deallocates memory */
void vec2dDestroy(vec2d* inVec);

/* I/O functions */
/* Write to stdout*/
int vec2dWrite(vec2d* x);

/* Write to file */
int vec2dWriteFile(vec2d* x, const char* outfile); 
/* Linear sum function */
/* x = a*y + b*z */
int vec2dLinearSum(vec2d* x, double a, vec2d* y,
		double b, vec2d* z );

/* Scale function */
/* x = a*x */
int vec2dScale(vec2d* x, double a);

/* Vector copy */
/* x = y */
int vec2dCopy(vec2d* x, vec2d* y);

/* Vector Constant */
/* sets all entries to a */
int vec2dConstant(vec2d* x, double a);

/* Scalar values */
double vec2dMin(vec2d* x);
double vec2dMax(vec2d* x);
double vec2dDot(vec2d* x, vec2d* y);
double vec2dTwoNorm(vec2d* x);
double vec2dRmsNorm(vec2d* x);
double vec2dMaxNorm(vec2d* x);

#endif
