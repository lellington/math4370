/* Daniel R. Reynolds
   SMU Mathematics
   Math 4370/6370
   27 January 2015 */

/* Modified by Layton Ellington for 
 * Homework2 */

/* Inclusions */
#include <stdlib.h>
#include <stdio.h>
#include "vec2d_b.h"
#include "get_time.h"

/* prototype for Gram-Schmidt routine */
int GramSchmidt2d_b(vec2d_b** X, int numvectors);


/* Example routine to test the vec2d_b "class" using * type memory */
/* Index [i][j] can be reached using for [i*n + j] */
int main() {
  int i, j, ier;

  /* create some vecs of length 5, and set some entries */
  vec2d_b *a = vec2d_bNew(5,5);
  vec2d_b *b = vec2d_bNew(5,5);
  vec2d_b *c = vec2d_bNew(5,5);

  for (i=0; i<5; i++){
    for(j = 0; j< 5; j++){
      b->data[(i*b->n)+j] = (i+1)*0.1;
    }
  }  

  for (i=0; i<5; i++){
    for(j = 0; j< 5; j++){
      c->data[(i*c->n)+j] = (i+1);
    }
  }  

  /* output to screen */
  printf("writing array of zeros:\n");
  vec2d_bWrite(a);
  printf("writing 2dvec of rows of  0.1, 0.2, 0.3, 0.4, 0.5:\n");
  vec2d_bWrite(b);
  printf("writing 2dvec of rows of 1, 2, 3, 4, 5:\n");
  vec2d_bWrite(c);

  /* verify that b has length 5 */
  if (b->m * b->n != 25)  printf("error: incorrect vector length\n");

  /* update a's data and write to file */
  a->data[0] = 10.0;
  a->data[1*a->n] = 15.0;
  a->data[2*a->n] = 20.0;
  a->data[3*a->n] = 25.0;
  a->data[4*a->n] = 30.0;
  vec2d_bWriteFile(a, "a_data");
  printf("the new file 'a_data' on disk should have first in row  entries 10, 15, 20, 25, 30\n\n");
  
  /* access each entry of a and write to screen */
  printf("entries of a, one at a time: should give 10, 15, 20, 25, 30, and zero on non first entries\n");

  for (i=0; i<5; i++){
    for(j = 0; j< 5; j++){
      printf(" %g\n", a->data[(i*a->n)+j]);
    }
  }  
  printf("\n");

  /* Test arithmetic operators */
  printf("Testing vector constant, should all be -1\n");
  ier = vec2d_bConstant(b, -1.0);
  vec2d_bWrite(b);

  printf("Testing vector copy, should be 1, 2, 3, 4, 5\n");
  ier = vec2d_bCopy(a, c);
  vec2d_bWrite(a);

  printf("Testing scalar multiply, should be 5, 10, 15, 20, 25\n");
  ier = vec2d_bScale(c, 5.0);
  vec2d_bWrite(c);

  /* create a few vecs of length 10 */
  vec2d_b* X[5];
  for (i=0; i<5; i++)
    X[i] = vec2d_bNew(10,10);
  
  /* fill in the vectors */
  for (i=0; i<10; i++) {
    for(j = 0; j<10; j++){
      X[0]->data[(i*X[0]->n)+j] = 1.0*i;
      X[1]->data[(i*X[1]->n)+j] = -5.0 + 1.0*i;
      X[2]->data[(i*X[2]->n)+j] = 2.0 + 2.0*i;
      X[3]->data[(i*X[3]->n)+j] = 20.0 - 1.0*i;
      X[4]->data[(i*X[4]->n)+j] = -20.0 + 1.0*i;
    }
  }
  
  /* check the LinearSum routine */
  ier = vec2d_bLinearSum(X[0], -2.0, X[1], 1.0, X[2]);
  printf("Testing LinearSum, should be all 12:\n");
  vec2d_bWrite(X[0]);

  /* check the various scalar output routines */
  printf("Testing TwoNorm, should be 5\n");
  printf("  %g\n\n", vec2d_bTwoNorm(b));
 
  printf("Testing RmsNorm, should be 16.583\n");
  printf("  %g\n\n", vec2d_bRmsNorm(c));
  
  printf("Testing MaxNorm, should be 1\n");
  printf("  %g\n\n", vec2d_bMaxNorm(b));
  
  printf("Testing Min, should be 1\n");
  printf("  %g\n\n", vec2d_bMin(a));
  
  printf("Testing Max, should be 25\n");
  printf("  %g\n\n", vec2d_bMax(c));
  
  printf("Testing Dot, should be should be 1375\n");
  printf("  %g\n\n", vec2d_bDot(a,c));
  
  printf("Testing Linspace, should be 0 1 2 3 4 over cols\n");
  vec2d_b *d = vec2d_bLinspace(0.0, 4.0, 5, 5);
  vec2d_bWrite(d);
  
  printf("Testing Random\n");
  vec2d_b *f = vec2d_bRandom(5,5);
  vec2d_bWrite(f);
  
  /*** performance/validity tests (Gram-Schmidt process) ***/
  int count = 0;
  /* lists of n and m for various sized testing */
  int* ms = (int[6]){10000, 1000, 100, 10, 100000, 1000000};
  int* ns = (int[6]){1000, 10000, 100000, 1000000, 100, 10};

  /* begin outer loop, iterating for each size pair*/
  printf("Begin GramSchmidt2d of various sizes\n");
  for(count = 0; count < 6; count++){
    printf("Running GramSchmidt2d_b process on %d by %d vecs\n", ms[count], ns[count]);

    vec2d_b** Y = (vec2d_b**) malloc(5 * sizeof(vec2d_b*));
    /* Create five random m by n vecs for test*/
    for (i=0; i<5; i++){
      Y[i] = vec2d_bRandom(ms[count],ns[count]);

    }
    double stime = get_time();
    if (GramSchmidt2d_b(Y, 5)){
      printf("GramSchmidt1d returned error\n");
    }
    double ftime = get_time();
    double rtime = ftime-stime;
    printf("Resulting vectors should be orthonormal:\n");
    for (i=0; i<5; i++){
      for (j=i; j<5; j++){
        printf("  <Y[%i],Y[%i]> = %g\n", i, j, vec2d_bDot(Y[i],Y[j]));
      }
    }
    printf("\n");
    printf("testing time: %g\n", rtime);
    
    /*clean up before looping*/
    for (i=0; i<5; i++){
      vec2d_bDestroy(Y[i]);
    }
    free(Y);

  }
  printf("Finshed GramSchmidt procceses\n");

  /* clean up */
  vec2d_bDestroy(a);
  vec2d_bDestroy(b);
  vec2d_bDestroy(c);
  vec2d_bDestroy(d);
  vec2d_bDestroy(f);
  for (i=0; i<5; i++)
    vec2d_bDestroy(X[i]);
  
  return 0;
} /* end main */

