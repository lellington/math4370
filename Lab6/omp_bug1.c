/* FILE: omp_bug1.c
   DESCRIPTION:
      This example attempts to show use of the parallel for construct.  
      However it will generate errors at compile time.  Try to determine 
      what is causing the error.  See omp_bug1fix.c for a corrected 
      version.
   AUTHOR: Blaise Barney  5/99
   UPDATED: Daniel R. Reynolds, 1/13/2013 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define N           50
#define CHUNKSIZE   5

int main (int argc, char *argv[]) {

  /* local variables */
  int i, chunk, tid, firstitr;
  float a[N], b[N], c[N];

  /* Some initializations */
  for (i=0; i < N; i++)  a[i] = b[i] = i * 1.0;
  chunk = CHUNKSIZE;

  /* Set iteration flag for first time proccess*/
  firstitr = 1;

# pragma omp parallel for  \
  shared(a,b,c,chunk)	   \
  private(i,tid,firstitr)  \
  schedule(static,chunk)
    for (i=0; i < N; i++) {
      if(firstitr == 1){
        tid = omp_get_thread_num();
	firstitr = 0;
      }
      c[i] = a[i] + b[i];
      printf("tid= %d i= %d c[i]= %f\n", tid, i, c[i]);
    }
   /* end of parallel for construct */

  return 0;
} /* end main */

