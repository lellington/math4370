# Wesley Ellington
# Math 4370

# Lab 4

#########################################
### Prime Number Generator Complexity ###
#########################################

Below is timing data for various input sizes run in exercises 1 and 2

n = 200
	real 0.23
	user 0.21
	sys 0.01

n = 300
	real 0.44
	user 0.41
	sys 0.01

n = 2000
	real 7.71
	user 7.38
	sys 0.32

n = 3000
	real 14.35
	user 13.76
	sys 0.59

n = 10000
	real 90.06
	user 86.58
	sys 3.54

n = 20000
	real 261.55
	user 251.79
	sys 9.96

n = 30000
	real 485.31
	user 467.21
	sys 18.49

###########################
### Complexity Analysis ###
###########################

Since system time is a function of node load and task switching, it will not 
be taken into account here. We will only look the user time needed for each 
calculation.

Running some quick calculations by hand, I noticed a trend relating scaling 
factor 10 on n (i.e. n vs 10n) and saw that the associated times grew by a 
factor of roughly 34 consistantly for each jump. This leads me to believe that
time complexity of this algorithm is linear, where the C value 
(arbirary constant) is close to 34. Although the conversion is not one to one
we can see that the scaling remains constant as n increases, giving us a 
simple linear function. This means our function is of order O(n).
