Wesley Ellington
Math 4370

Tread count and runtime informaiton for Lab5

Threads	Runtime	Speedup
1	5.7525	0% (serial execution)	
2	2.8005	205.41%
3	1.9915	288.85%
4	1.5141	379.93%
5	1.2608	456.26%
6	1.1442	502.75%
7	1.0062	571.71%
8	0.9770	588.79%
9	0.8686	662.27%
10	0.8601	668.82%
11	0.8334	690.24%
12	0.8150	705.83%
13	0.7433	773.91%
14	1.2125	474.43%
15	1.2227	470.48%
16	1.1953	480.26%

There are a few interesting things to note here. First of all, we can see
a classic taper of in speed increases as we approach 13 threads, as should
be expected in any type of speed up analysis with sufficiently large thread
count. Secondly, we see a sharp drop in performance between 13 and 14
threads, probably cause by some sort of data dependency within the code being
tested that only shows up once the data set has been decomposed into small 
pieces.
