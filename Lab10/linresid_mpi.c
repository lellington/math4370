/* Daniel R. Reynolds
   SMU Mathematics
   Math 4370/6370
   19 March 2015 */

/* Inclusions */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "mpi.h"

/* Description: calculates the linear residual and its averaged 2-norm (WRMS) */
int linresid(double *a, double *b, double *c, double *u, 
	     double *r, double *res, double *norm2, int m, MPI_Comm comm) 
  {

  /* local variables */
  int k, nprocs, my_id, ierr, kstart, kend, global_N;
  
  /* communication variables */
  double ul, ur, sl, sr, final_norm;

  MPI_Status stat;
  MPI_Request sendReqL, sendReqR, recvReqL, recvReqR;

  ierr = MPI_Comm_size(comm, &nprocs);
  if(ierr){
    fprintf(stderr,"Failed to get communcation group size\n");
    return 1;
  }
  
  ierr = MPI_Comm_rank(comm, &my_id);
  if(ierr){
    fprintf(stderr, "Failed to get Rank!\n");
    return 1;
  }
  
  /* calculate the begining location of each tasks work*/
  kstart = 0;

  /* caclulate the end location for each task*/
  kend = m - 1;

  /* hand left most value to previous process if not first*/
  if(my_id != 0){
    sl = u[kstart];
    MPI_Isend(&sl, 1, MPI_DOUBLE, my_id - 1, 1, comm, &sendReqL);
  }

  /* hand right most value to next process if not last*/
  if(my_id != nprocs - 1){
    sr = u[kend];
    MPI_Isend(&sr, 1, MPI_DOUBLE, my_id + 1, 1, comm, &sendReqR);
  }
  /* receive value from left if not first */
  if(my_id != 0){
    MPI_Irecv(&ul, 1, MPI_DOUBLE, my_id - 1, 1, comm, &recvReqL);
  }

  /* receive value from right if not last */
  if(my_id != nprocs - 1){
    MPI_Irecv(&ur, 1, MPI_DOUBLE, my_id + 1, 1, comm,  &recvReqR);
  }
  
  /* compute residual at start of subdomain*/
  if(my_id != 0){
    ierr = MPI_Wait(&recvReqL, &stat);
    res[kstart] = a[kstart]*ul + b[kstart]*u[kstart] + c[kstart]*u[kstart+1] - r[kstart];
    *norm2 = res[kstart]*res[kstart];
  }
  /* compute resudual at left end of entire domain */
  else{
    res[kstart] = b[kstart]*u[kstart] + c[kstart]*u[kstart+ 1] - r[kstart];
    *norm2 = res[kstart]*res[kstart];
  } 

  /* compute linear residual in interior of subdomain not including endpoints */
  for (k=kstart + 1 ; k<kend-1; k++) {
    res[k] = a[k]*u[k-1] + b[k]*u[k] + c[k]*u[k+1] - r[k];
    *norm2 += res[k]*res[k];
  }

  /* comput resudual at end of subdomain */
  if(my_id != nprocs -1 ){
    ierr = MPI_Wait(&recvReqR, &stat);
    res[kend] = a[kend]*u[kend-1] + b[kend]*u[kend] + c[kend]*ur - r[kend];
    *norm2 += res[kend]*res[kend];
  }
  /* compute linear residual at right end of entire domain */
  else{
    res[kend] = a[kend]*u[kend-1] + b[kend]*u[kend] - r[kend];
    *norm2 += res[kend]*res[kend];
  }

  /* reduce norm */
  MPI_Allreduce(norm2, &final_norm, 1, MPI_DOUBLE, MPI_SUM, comm);
  
  /* get proper problem size */
  MPI_Allreduce(&m, &global_N, 1, MPI_INT, MPI_SUM, comm);
  /* compute averaged 2-norm */
  
  *norm2 = sqrt(final_norm)/(global_N) ;
  
  return 0;
} /* end linresid */
