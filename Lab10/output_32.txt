TACC: Starting up job 8419204
TACC: Setting up parallel environment for MVAPICH2+mpispawn.
TACC: Starting parallel tasks...
iterative test with 32 processors
    gamma = 0.1
    linear solver tolerance delta = 1e-10
    global problem size N = 10000000
    local problem sizes n = 312500
 initial residual: ||T*u-r||_2 = 0.000316227
 converged in 9 iterations at delta = 1e-10
 solution time: 0.0993662 seconds
 final residual: ||T*u-r||_2 = 3.13786e-11
 
TACC: Shutdown complete. Exiting.
