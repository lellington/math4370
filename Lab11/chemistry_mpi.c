/* Daniel R. Reynolds
   SMU Mathematics
   Math 6370
   20 January 2011 */

/* Inclusions */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mpi.h"

/* Prototypes */
void chem_solver(double, double*, double*, double*, 
		 double, double, int, int*, double*);


/* Example routine to compute the equilibrium chemical densities at 
   a number of spatial locations, given a (random) background temperature
   field.  The chemical rate equations and solution strategy are in the 
   subroutine chem_solver, which is called at every spatial location. */
int main(int argc, char* argv[]) {

  /* declarations */
  int maxit, n, i, its, ierr, numprocs, myid, more_work;
  int sendto, tag, numsent, numstops;
  double T_loc, u_loc, v_loc, w_loc;
  double lam, eps, *T, *u, *v, *w, res, runtime;
  double stime, ftime;
  double *Sbuf, *Pbuf;
  MPI_Status status;

  /* Initialize MPI */
  ierr = MPI_Init(&argc, &argv);
  if (ierr != MPI_SUCCESS) {
     fprintf(stderr,"Error in calling MPI_Init\n");
     return 1;
  }

  ierr = MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  /* 1. set solver input parameters */
  maxit = 1000000;
  lam = 1.e-2;
  eps = 1.e-10;

  /* allocate communication buffers */
  Pbuf = malloc(4 * sizeof(double));
  Sbuf = malloc(3 * sizeof(double));

  /* begin master tasks */
  if (myid == 0){

    /* 2. input the number of intervals */
    printf("Enter the number of intervals (0 quits):\n");
    i = scanf("%i", &n);
    if (n < 1 || i < 1) {
      return(-1);
    }

    /* 3. allocate temperature and solution arrays */
    T = malloc( n * sizeof(double) );
    u = malloc( n * sizeof(double) );
    v = malloc( n * sizeof(double) );
    w = malloc( n * sizeof(double) );

    /* 4. set random temperature field, initial guesses at chemical densities */
    for (i=0; i<n; i++)  T[i] = random() / (pow(2.0,31.0) - 1.0);
    for (i=0; i<n; i++)  u[i] = 0.35;
    for (i=0; i<n; i++)  v[i] = 0.1;
    for (i=0; i<n; i++)  w[i] = 0.5;

    /* 5. start timer */
    stime = MPI_Wtime();


    /* Distribute intial work to workers */
    numsent = 1;
    for(i = 0; i < numprocs - 1; i++){
      /* create problem send buffer */
      Pbuf[0] = T[i];
      Pbuf[1] = u[i];
      Pbuf[2] = v[i];
      Pbuf[3] = w[i];

      /* send data to each worker */
      //printf("Master sent iteration %i to %i\n", i, i + 1);
      ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, i + 1, i + 1, MPI_COMM_WORLD);
      numsent ++; 
    }

    /* wait for response from workers */
    more_work = 1;
    numstops = 0;
    while(more_work){
      /* receive message from worker */
      ierr = MPI_Recv(Sbuf, 3, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status); 
      /* get tag for iteration value */
      tag = status.MPI_TAG;
      sendto = status.MPI_SOURCE;      

      //printf("Master received iterations %i from %i\n", tag, sendto);
      /* store sln information to solution vectors */
      u[tag - 1] = Sbuf[0];
      v[tag - 1] = Sbuf[1];
      w[tag - 1] = Sbuf[2];

      /* check to see if more work is available */
      if(numsent <= n){
        /* create problem buffer for next iteration */
        Pbuf[0] = T[numsent - 1];
        Pbuf[1] = u[numsent - 1];
        Pbuf[2] = v[numsent - 1];
        Pbuf[3] = w[numsent - 1];

        /* send next iteration to worker */
        ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, sendto, numsent, MPI_COMM_WORLD);
        numsent ++;
      }
      else{
        /* send garbage data with stop flag */
        ierr = MPI_Send(Pbuf, 4, MPI_DOUBLE, sendto, 0, MPI_COMM_WORLD);
        numstops ++;
      }

      /* if stop has been sent to all workers, exit loop */
     if(numstops == numprocs - 1){
       more_work = 0;
     }
      
    } /* end response system */
    

    /* 7. stop timer */
    ftime = MPI_Wtime();
    runtime = ftime - stime;

    /* 8. output solution time */
    printf("     runtime = %.16e\n",runtime);

    /* 9. free temperature and solution arrays */
    free(T);
    free(u);
    free(v);
    free(w);

    /* end master tasks */
  }


  /* start worker tasks */
  else {
    more_work = 1;
    /* continue untill stop flag is received */
    while(more_work){
      ierr = MPI_Recv(Pbuf, 4, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
   
      /* check if comm was successful */
      if(ierr != MPI_SUCCESS){
        fprintf(stderr, "Worker communication Error!\n");
        ierr = MPI_Abort(MPI_COMM_WORLD, 1);
      }
      
      /* check flag */
      tag = status.MPI_TAG;
      //printf("%i Received tag %i\n", myid, tag);
      if(tag != 0){
        /* continue doing work */

        /* set local variables for solve */
        T_loc = Pbuf[0];
        u_loc = Pbuf[1];
        v_loc = Pbuf[2];
        w_loc = Pbuf[3];

        /* solve interval */
        chem_solver(T_loc, &u_loc, &v_loc, &w_loc, lam, eps, maxit, &its, &res);

	/* set i to iteraton */
	i = tag - 1;

        /* print results */
        if (res < eps){ 
          //printf("    i = %i,  its = %i\n", i, its);
        }
        else {
          //printf("    error: i = %i,  its = %i,  res = %.2e,  u = %.2e,  v = %.2e,  w = %.2e\n", i, its, res, u[i], v[i], w[i]);
          return 1;
        }
 
        /* assign values to send buffer */
        Sbuf[0] = u_loc;
        Sbuf[1] = v_loc;
        Sbuf[2] = w_loc;

        /* send returned values to master */
        ierr = MPI_Send(Sbuf, 3, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
        //printf("%i sent iteration %i to master\n", myid, tag);
      }
      else{
        /* set stop flag */
        more_work = 0;
      }

    /* end while */
    } 
 
  /* end worker tasks */
  }

  /* finalize MPI */
  ierr = MPI_Finalize();

  return 0;
} /* end main */

